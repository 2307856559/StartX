# StartX （已更新至v1.2.1）


<br/>

### 版本更新 
<br/>

	- v1.0.0 支持http(s)
	- v1.1.0 支持ws(s)
	- v1.1.1 支持http(s)+ws(s)
	- v1.2.1 支持文本socket
	
<br/>

### 项目简介  

<br/>

Netty和Spring开发的服务端容器，支持Socket，WebSocket(SSL)，HTTP(S)，提供集成环境，能很快速的进入业务开发，对业务开发者透明，支持对各种通讯协议进行定制

<br/>

### 源码结构

<br/>

```
├─src
│  ├─main
│  │  ├─java
│  │  │  └─com
│  │  │      └─startx
│  │  │          └─core
│  │  │              ├─config                          #系统配置
│  │  │              │  ├─holder
│  │  │              │  └─reader
│  │  │              │      └─impl
│  │  │              ├─netty                           #网络通讯
│  │  │              │  ├─handler
│  │  │              │  ├─output
│  │  │              │  │  ├─http
│  │  │              │  │  ├─socket
│  │  │              │  │  └─wss
│  │  │              │  ├─pipeline
│  │  │              │  └─server
│  │  │              │      └─impl
│  │  │              ├─point                            #MVC框架
│  │  │              │  ├─anotation
│  │  │              │  └─factory
│  │  │              ├─system                           #系统变量
│  │  │              │  ├─constants
│  │  │              │  ├─model
│  │  │              │  └─param
│  │  │              └─tools                            #系统工具
│  │  ├─resources                                       #资源文件
│  │  └─test                                            #测试代码
│  │      └─com
│  │          └─startx
│  │              ├─action
│  │              └─test
```
<br/>

### 启动方式

<br/>

- 导入maven项目
- 打开Bootrap类
- 新建一个测试类
- 创建main函数
- 调用Bootstrap.start()

<br/>

### 修改配置

<br/>

```
#项目启动端口
port=8081
#spring配置路径
springPath=classpath*:application.xml
#项目访问前缀（已实现）
endPoint=/j4
#是否开启ssl
isSSL=false
#若开启ssl，需要配置如下两个参数
jksPwd=your.pwd
jksPath=/your.jks
#配置使用websocketServer，同时支持ws(s)和http(s)
serverClass=com.startx.core.netty.server.impl.WebSocketServer
```

<br/>

### 更多配置

<br/>

```
/**
 * 监听端口
 */
private int port = 8080;
/**
 * boss线程数
 */
private int boss = 2;
/**
 * workder线程数
 */
private int worker = 4;
/**
 * spring配置路径
 */
private String springPath = "classpath*:application.xml";
/**
 * 项目访问前缀
 */
private String endPoint = "/";
/**
 * 是否为ssl
 */
private boolean isSSL = false;
/**
 * 静态资源目录
 */
private String resource = "/resource";
/**
 * endpoint扫描路径
 */
private String packages = "com.startx";
/**
 * 设置NettyServer启动类
 */
private String serverClass = "com.startx.core.netty.server.impl.HttpServer";
// 以下配置在ssl连接时使用
/**
 * jsk密码
 */
private String jksPwd;
/**
 * jsk路径
 */
private String jksPath;
```

<br/>

### 代码测试

<br/>

打开演示代码J4.java,右键即可运行

```
package com.startx.test;

import com.startx.core.Bootstrap;

public class J4 {
	
	public static void main(String[] args) throws Exception {
		
		Bootstrap.start();
		
	}
	
}
```

<br/>

### 日志打印

<br/>

```
2018-05-09 13:06:05 INFO  com.startx.core.netty.server.impl.HttpServer  - StartxConfig [port=8081, boss=2, worker=4, springPath=classpath*:application.xml, endPoint=/, isSSL=false, resource=/resource, jksPwd=your.pwd, jksPath=/your.jks, packages=com.startx, serverClass=com.startx.core.netty.server.impl.HttpServer]
2018-05-09 13:06:05 INFO  org.springframework.context.support.ClassPathXmlApplicationContext  - Refreshing org.springframework.context.support.ClassPathXmlApplicationContext@4bec1f0c: startup date [Wed May 09 13:06:05 CST 2018]; root of context hierarchy
2018-05-09 13:06:05 INFO  org.springframework.beans.factory.xml.XmlBeanDefinitionReader  - Loading XML bean definitions from URL [file:/E:/zhangminghu/startx/target/classes/application.xml]
2018-05-09 13:06:05 INFO  com.startx.core.accesspoint.factory.AccessPointFactory  - GET_/abc/def2_JSON
2018-05-09 13:06:05 INFO  com.startx.core.accesspoint.factory.AccessPointFactory  - GET_/abc/def3_JSON
2018-05-09 13:06:05 INFO  com.startx.core.accesspoint.factory.AccessPointFactory  - GET_/abc/def4_JSON
2018-05-09 13:06:05 INFO  com.startx.core.accesspoint.factory.AccessPointFactory  - GET_/abc/def1_JSON
2018-05-09 13:06:06 INFO  com.startx.core.netty.server.impl.HttpServer  - 
**************************    *********                     ******  ********************************
**************************  ***********                     ******  ********************************
**************************  ********* **************************************************************
**************************  ***   **************************************** *************************
**************************  ***     ********             *********     *** *************************
**************************   ***                                      **** *************************
**************************   ***                                      ***  *************************
**************************  ***                                       ***  *************************
**************************  ***                                        *** *************************
************************** ****                                        *** *************************
************************** ***                                         *** *************************
************************** ***     *****                    ****       *** *************************
************************** ***    ********               *********      ****************************
*****************************      *******  *****  ***** *********      ****************************
*****************************        **** ***********************       ****************************
*****************************            *******************            ****************************
****************************             *******************             ***************************
****************************             *******************             ***************************
****************************             *******************             ***************************
****************************              *****************              ***************************
****************************                ************                 ***************************
**************************  ***                                        *** *************************
**************************   ***                V1.1.1                ***  *************************
**************************   ****                                     **   *************************
****************************  ***   ******************************   ***   *************************
**********************************  ************************************   *************************
**************************************  *******************************    *************************
******************************* ******                        *******      *************************
2018-05-09 13:06:06 INFO  com.startx.core.netty.server.impl.HttpServer  - 服务已启动，端口号： 8081.
```
<br/>

### 业务代码

<br/>

```
package com.startx.action;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;

import com.startx.core.accesspoint.anotation.AccessPoint;
import com.startx.core.accesspoint.anotation.RequestMethod;
import com.startx.core.accesspoint.anotation.RequestPoint;
import com.startx.core.accesspoint.anotation.ResponseType;
import com.startx.core.netty.output.http.JsonOutput;
import com.startx.core.system.Colorfulogo;
import com.startx.core.system.param.HttpArgs;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.HttpResponseStatus;

@Controller
@AccessPoint("/login")
public class LoginAction {
	
	@RequestPoint(value={"/empty/param"},method=RequestMethod.GET,type=ResponseType.JSON)
	public void emptyParam() {
		
		System.out.println("login...");
		
	}
	
	@RequestPoint(value={"/body/ctx/args"},method=RequestMethod.GET,type=ResponseType.JSON)
	public void test02(byte[] body,ChannelHandlerContext ctx,HttpArgs args) throws UnsupportedEncodingException {
		
		Map<String,Object> response = new HashMap<>();
		response.put("colorfulogo", Colorfulogo.get());
		JsonOutput.object(ctx, HttpResponseStatus.OK,response);
		
	}
	
	@RequestPoint(value={"/args/body"},method=RequestMethod.GET,type=ResponseType.JSON)
	public void test03(HttpArgs args,byte[] body) {
		
		System.out.println("login...");
		
	}
	
	@RequestPoint(value={"/args"},method=RequestMethod.GET,type=ResponseType.JSON)
	public Map<String,Object> test04(HttpArgs args) {
		
		System.out.println("login... ");
		Map<String,Object> response = new HashMap<>();
		response.put("colorfulogo", Colorfulogo.get());
		
		return response;
	}
	
}

```

<br/>

### 打包部署（未完待续）












